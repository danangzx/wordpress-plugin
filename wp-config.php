<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'medisite' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'I?gh::!7QVL_Kj=$UQ]9[#A(@lnd=E!52m6JR%%Ig]cKU(CbLC#qqN}LM=F#YT24' );
define( 'SECURE_AUTH_KEY',  'g92VGoq`DlcKNWiza%#fn+_m^fc4>U~1DVBifB_<xPl  `NhN*4,lpkf9Tc~)aT,' );
define( 'LOGGED_IN_KEY',    'eE[*i_u(b97A#>na4tffMpibKC<WDhKH{pcvA}n^w*PlnhaomJG~?I|B#A|c2zSP' );
define( 'NONCE_KEY',        ' k``v]3kvH6c6N5&tg)M-VCj{ %nt}lj/_Gr35DrOx1>nHRY8)Fa-8d.2*a&PeLs' );
define( 'AUTH_SALT',        '9GDQiEy;$??BkKn2ptnonN/1)`e*ATR#TsU;nKn3%x</o&!!C<;3o;kV%!D$%`E^' );
define( 'SECURE_AUTH_SALT', '[K<x|H`zoypmA9i`ANe_]DdPg/&:7jp|cBXv;haj ,Aqy]D8Bjs1T?<qqyVB!^y(' );
define( 'LOGGED_IN_SALT',   ';sK)8uuC`X +xlEj{>oe(/{8?q(N?pdd>c,nVW<4)-G dgRmnk& kgGT#SYMREuU' );
define( 'NONCE_SALT',       '3Gmggq^`C >SjQroP0[09J__D+LtD>T^?rv?O#l3l] ,1wCyY<]EzpY:&6}$W/|b' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
