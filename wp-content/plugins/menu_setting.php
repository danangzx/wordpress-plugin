<?php
/**
 * @package Akismet
 */
/*
Plugin Name: plugins Menu Setting Json
Plugin URI: http://localhost/wordpress/
Description: Setting Json
Version: 4.1.8
Author: Automattic
Author URI: https://automattic.com/wordpress-plugins/
*/

//membuat menu di admin
add_action('admin_menu','medisite_menu_action');


function medisite_menu_action(){
    add_menu_page('Medisite Plugin','Medisite Setting','administrator',__FILE__,'medisite_setting_page');
}

//isi menu admin
function medisite_setting_page(){
    if(array_key_exists('btn_submit', $_POST)) { 
        update_url_json();
         
    }
    include('koneksi.php');
    $query1 = mysqli_query($koneksi,'SELECT setting_value FROM wp_settings where setting_key = "DATA_URL"');
    $row1=mysqli_fetch_array($query1);
    ?>
    <h1>Setting Url json</h1>
    <div>
        <p>Selamat Datang di Pengaturan URL Json medisite</p>
    </div>
    <div>
        <table border="1px">
            <thead>
                <tr>
                    <th>Link Yang Terset sekarang</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $row1['setting_value']; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div>
        <form method="POST">
            <label >Input Url json</label>
            <div><input type="text" name="input_url" ></div>

            <div>
                <input type="submit" name="btn_submit">
            </div>
        </form>
    </div>
    
    <?php
    
    view_users();
    
}

function update_url_json(){
    include('koneksi.php');

    $link_json = $_POST['input_url'];

    mysqli_query($koneksi,"UPDATE wp_settings SET setting_value = '$link_json' WHERE setting_key = 'DATA_URL'");

    assets_js();
    
}

//Load Js
function assets_js(){
    wp_enqueue_script('my-js',plugins_url('alert.js',__FILE__));
}


add_shortcode('external_data','view_users');
function view_users(){
    
    ob_start();
    include_once('koneksi.php');
    $query1 = mysqli_query($koneksi,'SELECT setting_value FROM wp_settings where setting_key = "DATA_URL"');
    $row1=mysqli_fetch_array($query1);

    // contoh $json = 'http://localhost/sesuatu0.json';
    $json = $row1['setting_value']; //ngambil database
    $url = $json;
    
    //read json
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    $response  = curl_exec($ch);

    if($e = curl_error($ch)){
        echo $e;
    }else{
        $result = json_decode($response);
    }
    curl_close($ch);

    ?>
    <h2>Biodata user</h2>
    <table border="1px">
        <thead>
            <tr>
                <th>id.</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Email</th>
                <th>Address</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
                foreach($result as $result){
                    echo "
                    <tr>
                     <td>".$no++.".</td>
                     <td>".$result->name."</td>
                     <td>".$result->gender."</td>
                     <td>".$result->email."</td>
                     <td>".$result->address."</td>
                    </tr>
                    ";
                }
            ?>
        </tbody>
    </table>
    <?php
    return ob_get_clean();
}